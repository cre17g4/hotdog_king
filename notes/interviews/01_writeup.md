# Hotdog KING

## Problem
At the moment HotDog King have a problem managing tasks:
- Some tasks are not being fulfilled
- Some tasks may be performed by several employees
- Some tasks are not being completed within a deadline
- No way of getting accurate statistics
- Not a structured way of defining and assigning tasks

## Goal
HotDog King wants an efficient task management system that:

- Keeps track of tasks and employees
    - Which employee is assigned to which task.
- Provides statistics of tasks being completed, missed deadlines etc.
- Works on different platforms
    - Some sort of touch screen should be placed on every work station
- Easy to use

## Stakeholders
The stakeholders that are going to be affected by the system includes:

- Top management
    - HQ & franchise managers
- Middle management
    - Team leaders
- Other
    - Employees
    - (Customers)

## Raw requirements / Functionality
The system should provide:

- A way of communicating inside of the system
- The ability to sort tasks on different status
    - Completed, in progress etc.
- The ability to label different tasks
    - For instance tasks that requires a certain certificate
- An overview of current tasks (statistics)
- The ability to manage tasks
    - Create new tasks, assigning tasks etc.
- Different interfaces depending on the users authorization level
    - The managers should be the ones who creates and assigns tasks
- The ability to upload/download files from the system
- Personal accounts for every user
    - Personal details, work role etc.
