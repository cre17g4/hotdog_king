# Creativity Workshop

## Hall of Fame

### Hillary Clinton

#### Features

- Corruption
- Use programm in congress to recruit people
- Surveilence
- Controll over employees to prevent leaks
- Sign in control inkl progress
- Predict all the things
- PR/Social Media integration
- Throw money at problems approach
- Deligate
- Automate report loop

#### New ideas

- Something that benifits her
- She would take over
- Battle Royal the employees
- Feminism
- Equality of workload

#### Needs

- Power, Control
- Justice
- Understand your enemy
- Quality Control
- Micromanage
- be the president

#### Random

- Look pretty/classy/professional
- Promise the customer what they want
- our client (trump) wants everything from us inkl. war
- Bring the idea of family into the business
- Family oriented design
- Patriotism
- use Foundation/Charity as cover

### Spongebob

#### Features

- Feedback system (stars)
- Work overtime
- Impact own schedule/tasks
- Give people a second chance
- Communication
- Show appreciation to others
- Protect the secret recipy

#### New ideas

- Change hotdog to crabby patty
- Jelly fishing with staff
- change menu names
- complete redesign
- get patric to work with him
- bring your pet
- skip/reassign tasks

#### Random

- Clear instructions
- 9 to 5 attitude
- Routine
- Easy and practical
- Be wholesome
- be funny
- be childish
- Every dish should have jelly jam
- Get new products to customers
- Customize interface for employee
- Play music
- Improve team relatioionships
- loyality/whorshipp the burger
- IOT everything

### Rick Sanchez

- Automate everything
- Whubbalubadubdub
- System to run the system
- Tasks inside of tasks
- Everyone and everything is replaceable (and will die)
- Everything is customizable
- science can solve everything
- Do crazy shit that might get you killed

### Einstein

#### Random

- Innovation/inventions
- Experiments
- Customize everything
- Tasks at light speed (everything is relative in competition)
- Use scientific data to come to a conclusion
- Have a lab
- Have reminders

### User storie

- As a manager I want to put tasks into the system so they get done.

  - as CAPTAIN Jack Sparrot I want everyone to follow my command without a stupid IT system.
  - As Batman I want to stay in cover
  - As a wright brother I want to be able to invent new tasks.
  - As Emperor Merkel I want everything to stay the same. Change is scary.

- As an employee I want the system to tell me what to do.

  - As slatan I dont want the system to tell me what to do.
  - As Spongebob I want step by step instructions for my task so i cam complete it perfectly.
  - as Muhmamed Ali I want the system to tell my everything I need so I don't have to ask slatan.
  - As the Hulk I don't want the tasks to make me angry
  - As the other wright brother I want to be able to be the first to complete a task.
  - as M.E. I want the system to change according to my daily form.
  - As a manager I want an annual report related to stuffs work
  - As Bach I want the UI of the system looks more artistic

  ## Constraint Removal

#### Constraints

- Show Tasks on a screen
  - Blackboard
  - Speakers
  - Hologram
  - Headphone-thingy
  - AR
  - Mailperson
- How Login into the system
  - Face, voice, hand
  - RFID
  - Token/Key
  - Email
- That we have to login
  - Show tasks public
  - Personal delivery device
- No 24/7 surveilence
  - Connect GPS tracker to profile
  - Markers on uniforms so cams can identify them
  - Key cards
  - Rank each other (black mirror)
  - Check-in and check-out at stations
- Enter tasks manually (while being busy)
  - Voice command
  - ML predictor
  - QR code thingy
  - Predefined tasks
  - Call number
- Enter completion manually
  - Buttons for feet
  - QR code thingy
- Only management can enter tasks
  - Everybody can enter tasks
  - Predefined tasks list
  - ML voodoo
  - Deligate power
  - Above should have access


- Knowledge level has to fit

  - Tutorial
  - Offer support/Workshops

- Secruity Problems can arise from public terminals

  - Auto logout
  - keys to use system
  - Authorise actions
  - Log pictures during actions

- System has to fit many platforms

  - Web application
  - Dedicated Hardware
  - Just have one big screen that shows all information
  - ​

- System has to be always online

  - Synchronize every xy time
  - Backups
  - Distribution
  - UPS
  - just have offline thing
  - work with paper

- System relies on team leader for distribution

  - automated distribution
  - ML voodoo
  - self organized
  - future predictions

## Useful Requirements from workshop

To be used in interview no. 2

- Predict all the things
    - Recuring tasks should be predicted/automated
    - Let managers add recuring tasks
- Security
    - Protecting privileges? (cheating)
    - How to identify the user? (input - username and password only? biometrics?)
    - How to handle the login? (input - username and password only? biometrics?)
    - Backup and synchronization of data to HQ? (recovery)
    - what is the risks of data loss? (does that affect the goal?)
- Automate report loop
    - gather and offer all the data (Regualar reports?)
- Stats comparing employees and restaurants
    - Maybe give bonuses to the best or use the info to improve the process where there are problems.
    - This might incentivize better performance.
    - is this related to statistics?
- Quality control / micromanage
    - Clear instructions about how to do the tasks (Input and output data)
- equal workload
    - Should the system provide some kind of recommendation on who to assign different tasks?
    - would that be a problem for statistics or comparing employees?
- feedback and comments on tasks
    - Is this part of the communication system?
- everything is replaceable
    - can the tasks be re-assigned?
    - In what way should actors be able to interact with the tasks (other than touch screens)?
- Tasks inside of tasks
    - Worst case scenario? (what is the most complicated case of tasks)
    - Does different task contain other tasks? (Sub-tasks)
    - Does tasks overlap? relate? (Priority or certain sequence)?
- Have reminders
    - If tasks gets forgotten?
    - If a task is created and not assigned?
    - How should the system provide these reminders if any?
- Dedicated hardware?
    - do you need alternatives to touch screens / cross-platform thing?
    - with dedicated accouts? what about security? how do we keep track?
- How to make sure that an employee completed the assigned task?
    - would the employee ask his friends to complete his tasks? (cheating)


